from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'I Made Adisurya Nugraha' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,3,10) #TODO Implement this, format (Year, Month, Date)
npm = 1706984625 # TODO Implement this
school = "Universitas Indonesia"
hobi = "Bermain musik, berolahraga, belajar"
deskripsi = "Seorang mahasiswa Fasilkom UI 2017 yang ingin belajar PPW"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'school' : school,'hobby':hobi,'description':deskripsi }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
